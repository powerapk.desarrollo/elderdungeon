import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

/*
  Generated class for the UserPreferencesProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class UserPreferencesProvider {

  public uid:any = "";
  public email:any = "";

  constructor() {
    console.log('Hello UserPreferencesProvider Provider');
    
  }

  setUid(uidInfo:string){
    this.uid = uidInfo;
  }

  setEmail(emailInfo:string){
    this.email = emailInfo;
  }

}
