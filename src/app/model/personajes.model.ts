export interface Personajes{
    nombre: string;
    clase: string;
    id?: string;
    user: string;
    raza: string;
    recurso: string;
    rank: string;
    status: string;
    level: number;
}