import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';

import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { LoginPage } from '../pages/login/login';
import { RegistrarsePage } from '../pages/registrarse/registrarse';

import { AngularFireModule } from 'angularfire2';
import { AngularFirestoreModule } from "angularfire2/firestore";

import { AngularFireAuthModule } from 'angularfire2/auth';
import { FIREBASE_CONFIG } from './app.firebase.config';

import { IonicStorageModule } from '@ionic/Storage';
import { InventarioPage } from '../pages/inventario/inventario';
import { ArmaduraPage } from '../pages/armadura/armadura';
import { MisionesPage } from '../pages/misiones/misiones';
import { StatsPage } from '../pages/stats/stats';
import { GamePage } from '../pages/game/game';
import { UserPreferencesProvider } from '../providers/user-preferences/user-preferences';

@NgModule({
  declarations: [
    MyApp,
    HomePage,
    LoginPage,
    RegistrarsePage,
    InventarioPage,
    ArmaduraPage,
    MisionesPage,
    StatsPage,
    GamePage,
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
    IonicStorageModule.forRoot(),
    AngularFireModule.initializeApp(FIREBASE_CONFIG),
    AngularFireAuthModule,
    AngularFirestoreModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    LoginPage,
    RegistrarsePage,
    InventarioPage,
    ArmaduraPage,
    MisionesPage,
    StatsPage,
    GamePage,
  ],
  providers: [
    StatusBar,
    SplashScreen,
    { provide: ErrorHandler, useClass: IonicErrorHandler },
    UserPreferencesProvider,
  ]
})
export class AppModule { }
