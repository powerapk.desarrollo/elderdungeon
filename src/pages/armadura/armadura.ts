import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController } from 'ionic-angular';
import { AngularFirestore } from 'angularfire2/firestore';
import { AngularFireAuth } from 'angularfire2/auth';
import { UserPreferencesProvider } from '../../providers/user-preferences/user-preferences';

/**
 * Generated class for the ArmaduraPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-armadura',
  templateUrl: 'armadura.html',
})
export class ArmaduraPage {

  uid:any;

  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    public viewCtrl: ViewController,
    private angularFirestore: AngularFirestore,
    public afAuth: AngularFireAuth,
    public userData: UserPreferencesProvider,
    ) {
      this.afAuth.authState.subscribe(data => {
        this.userData.uid = data.uid;
      });
    }

  ionViewDidLoad() {
    console.log(`ionViewDidLoad ArmaduraPage ${this.userData.uid}`);
  }

  cancel(){
    this.viewCtrl.dismiss();
  }

}
