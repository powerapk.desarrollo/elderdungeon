import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ArmaduraPage } from './armadura';

@NgModule({
  declarations: [
    ArmaduraPage,
  ],
  imports: [
    IonicPageModule.forChild(ArmaduraPage),
  ],
})
export class ArmaduraPageModule {}
