import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, Platform } from 'ionic-angular';
import { User } from '../../app/model/user.model';
import { AngularFireAuth } from '../../../node_modules/angularfire2/auth';
import { HomePage } from '../home/home';
import { RegistrarsePage } from "../registrarse/registrarse";

import { StatusBar } from '../../../node_modules/@ionic-native/status-bar';
import { UserPreferencesProvider } from '../../providers/user-preferences/user-preferences';

@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {

  user = {} as User

  //Variables del Usuario
  uid: any;
  email: any;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private afAuth: AngularFireAuth,
    private statusBar: StatusBar,
    private platform: Platform,
    public userData: UserPreferencesProvider,
  ) {
    this.initializeApp();
  }

  initializeApp() {
    this.platform.ready().then(() => {
      this.statusBar.hide();
    });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad LoginPage');
  }

  async login(user: User) {

    const result = this.afAuth.auth.signInWithEmailAndPassword(user.email, user.password).then((results) => {
      console.log("Succed");
      this.uid = results.user.uid;
      this.email = results.user.email;
      const token = results.user.getIdToken;
      this.navCtrl.setRoot(HomePage);
      console.log(
        "Guardando uID: " + this.uid +
        "Guardando Email: " + this.email +
        "Guardando Token: " + token);
      this.userData.setUid(this.uid)
      this.userData.setEmail(this.email);
    }).catch(function (error) {
      var errorCode = error.code;
      var errorMessage = error.message;
      console.error("Fail");
      console.error(errorCode);
      console.error(errorMessage);
    });

  }

  register() {
    this.navCtrl.push(RegistrarsePage);
  }

}
