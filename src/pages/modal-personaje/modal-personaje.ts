import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController } from 'ionic-angular';
import { Personajes } from '../../app/model/personajes.model';
import { AngularFirestore } from '../../../node_modules/angularfire2/firestore';

/**
 * Generated class for the ModalPersonajePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-modal-personaje',
  templateUrl: 'modal-personaje.html',
})
export class ModalPersonajePage {

  personaje: Personajes;

  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    public viewCtrl: ViewController,
    private angularFirestore: AngularFirestore
  ) {
      this.personaje = navParams.get('personaje');
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ModalPersonajePage');
    console.log(this.personaje);
  }

  cancel(){
    this.viewCtrl.dismiss();
  }

  actualziar(personaje: Personajes){
    
    this.viewCtrl.dismiss(personaje);
    
  }

}
