import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ModalPersonajePage } from './modal-personaje';

@NgModule({
  declarations: [
    ModalPersonajePage,
  ],
  imports: [
    IonicPageModule.forChild(ModalPersonajePage),
  ],
})
export class ModalPersonajePageModule {}
