import { NgModule, ErrorHandler } from '@angular/core';
import { IonicPageModule, IonicErrorHandler } from 'ionic-angular';
import { GamePage } from './game';
import { InventarioPage } from '../inventario/inventario';
import { ArmaduraPage } from '../armadura/armadura';
import { MisionesPage } from '../misiones/misiones';
import { StatsPage } from '../stats/stats';
import { IonicStorageModule } from '@ionic/Storage';
import { UserPreferencesProvider } from '../../providers/user-preferences/user-preferences';

@NgModule({
  declarations: [
    GamePage,
    InventarioPage,
    ArmaduraPage,
    MisionesPage,
    StatsPage,
  ],
  imports: [
    IonicPageModule.forChild(GamePage),
    IonicStorageModule.forRoot(),
  ],
  providers: [{provide: ErrorHandler, useClass: IonicErrorHandler},
    UserPreferencesProvider
  ]
})
export class GamePageModule {}
