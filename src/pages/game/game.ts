import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ModalController } from 'ionic-angular';
import { InventarioPage } from '../inventario/inventario';
import { ArmaduraPage } from '../armadura/armadura';
import { MisionesPage } from '../misiones/misiones';
import { StatsPage } from '../stats/stats';
import { UserPreferencesProvider } from '../../providers/user-preferences/user-preferences';
import { AngularFireAuth } from 'angularfire2/auth';

/**
 * Generated class for the GamePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-game',
  templateUrl: 'game.html',
})
export class GamePage {

  uid: any;

  result: number = 0;

  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    private userData: UserPreferencesProvider,
    public modalCtrl: ModalController,
    public afAuth: AngularFireAuth,) {
      this.afAuth.authState.subscribe(data => {
        this.userData.uid = data.uid;
      });
  }

  ionViewDidLoad() {
    console.log(`ionViewDidLoad GamePage ${this.userData.uid}`);
    
  }

  goInventario() {
    let contactModal = this.modalCtrl.create(InventarioPage,   { 'uid':  this.uid });
    contactModal.present();
  }

  goPersonaje() {
    let contactModal = this.modalCtrl.create(ArmaduraPage,   { 'uid':  this.uid });
    contactModal.present();
  }

  goMisiones() {
    let contactModal = this.modalCtrl.create(MisionesPage,   { 'uid':  this.uid });
    contactModal.present();
  }

  goPuntuaciones() {
    let contactModal = this.modalCtrl.create(StatsPage,   { 'uid':  this.uid });
    contactModal.present();
  }

  randomNumber(range) {
    return Math.round(Math.random() * range);
  }

  // --- d4 --- //
d4Roll() {
  var d4Result = Math.floor(Math.random() * 4 + 1);
  this.result = d4Result;
}


// --- d6 --- //
d6Roll() {
  var d6Result = Math.floor(Math.random() * 6 + 1);
  this.result = d6Result;
}

// --- d8 --- //
d8Roll() {
  var d8Result = Math.floor(Math.random() * 8 + 1);
  this.result = d8Result;
}

// --- d10 --- //
d10Roll() {
  var d10Result = Math.floor(Math.random() * 10 + 1);
  this.result = d10Result;
}

// --- d12 --- //
d12Roll() {
  var d12Result = Math.floor(Math.random() * 12 + 1);
  this.result = d12Result;
}

// --- d20 --- //
d20Roll() {
  var d20Result = Math.floor(Math.random() * 20 + 1);
  this.result = d20Result;
}

// --- d100 --- //
d100Roll() {
  var d100Result = Math.floor(Math.random() * 100 + 1);
  this.result = d100Result;
}

}
