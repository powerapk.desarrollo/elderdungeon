import { Component, ViewChild } from '@angular/core';
import { NavController, ToastController, ModalController, NavParams, AlertController, Platform, Content } from 'ionic-angular';
import { AngularFireAuth } from 'angularfire2/auth';
import { LoginPage } from '../login/login'

import { Observable } from 'rxjs/observable';
import { Personajes } from '../../app/model/personajes.model';
import { AngularFirestore, AngularFirestoreCollection } from 'angularfire2/firestore';
import { platformBrowser } from '../../../node_modules/@angular/platform-browser';
import { InventarioPage } from "../inventario/inventario";
import { ArmaduraPage } from '../armadura/armadura';
import { MisionesPage } from '../misiones/misiones';
import { StatsPage } from '../stats/stats';
import { GamePage } from '../game/game';
import { UserPreferencesProvider } from '../../providers/user-preferences/user-preferences';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  personaje = {} as Personajes

  personajesCollection: AngularFirestoreCollection<Personajes>;
  personajes: Personajes[];

  listSize: any;

  //Variables del Usuario
  uid: any;
  email: any;
  //Variables de estado UI
  registarStatus: boolean = false;
  fabStatus: boolean = true;
  infoStatus: boolean = true;
  content: any;
  //Variables de Personaje
  personajeRecurso: any;



  constructor(
    public navCtrl: NavController,
    private afAuth: AngularFireAuth,
    private toast: ToastController,
    public modalCtrl: ModalController,
    public navParams: NavParams,
    private angularFirestore: AngularFirestore,
    public alertController: AlertController,
    public userData: UserPreferencesProvider,
  ) {
  }

  ionViewDidEnter() {
    console.log('ionViewDidEnter HomePage');
  }

  goToRegister() {
    this.infoStatus = !this.infoStatus;
  }

  ionViewWillLoad() {

    console.log(`ionViewDidLoad HomePage ${this.userData.uid}`);
    this.afAuth.authState.subscribe(data => {
      if (data && data.email && data.uid) {
        this.userData.setUid(data.uid);
        this.userData.setEmail(data.email);
        
        this.personajesCollection = this.angularFirestore.collection('personajes', ref =>
          ref.where('user', '==', data.uid));
        this.personajesCollection.snapshotChanges().subscribe(personajesList => {
          this.personajes = personajesList.map(item => {
            return {
              id: item.payload.doc.id,
              nombre: item.payload.doc.data().nombre,
              clase: item.payload.doc.data().clase,
              user: item.payload.doc.data().user,
              raza: item.payload.doc.data().raza,
              recurso: item.payload.doc.data().recurso,
              rank: item.payload.doc.data().rank,
              status: item.payload.doc.data().status,
              level: item.payload.doc.data().level,
            }
          })
          this.listSize = personajesList.length;
          if (this.listSize == 0) {
            this.registarStatus = true;
          }
          console.log(this.listSize+this.userData.uid);
        })


        this.toast.create({
          message: `Welcom to ElderDungeon, ${data.email} de ${data.uid}`,
          duration: 3000
        }).present();

      }
      else {
        this.toast.create({
          message: 'No se pudo encontrar los detalles de autentificacion',
          duration: 3000
        }).present();
        this.navCtrl.setRoot(LoginPage);
      }
    });
  }

  async logOut() {
    console.log('LogOut');
    this.afAuth.auth.signOut()
      .then(() => this.navCtrl.setRoot(LoginPage));
  }

  async crear(personaje: Personajes) {
    switch (personaje.clase) {
      case 'Arquero':
        this.personajeRecurso = "Energy";
        break;

      case 'Guerrero':
        this.personajeRecurso = "Rage";
        break;

      case 'Mago':
        this.personajeRecurso = "Mana";
        break;

      default:
    }
    console.log('Creando Personaje');
    if (this.listSize == 0) {
      this.angularFirestore.collection('personajes').add({
        nombre: personaje.nombre,
        clase: personaje.clase,
        user: this.userData.uid,
        raza: personaje.raza,
        recurso: this.personajeRecurso,
        rank: '0',
        status: 'Normal',
        level: 1,
      }).then(newPersonaje => {
        console.log('Personaje' + personaje.nombre + 'se a creado');
      }).catch(error => {
        console.log(error);
      })
      this.registarStatus = false;
      this.personaje.nombre = "";
      this.personaje.clase = "";
      this.personaje.raza = "";
    } else {
      let alert = this.alertController.create({
        title: 'Just one Hero',
        message: 'Solo puedes crear un heroe , si deseas crear uno nuevo debes deshacerte del que ya tienes',
        buttons: [
          {
            text: 'OK',
            role: 'ok',
            handler: () => {
              console.log('ok');
            }
          }
        ]
      });
      alert.present();
    }
  }



  async ingresar() {
    this.userData.setUid(this.uid)
    console.log('Ingresando a ElderDungeon pag 1');
    console.log(this.userData.uid);
    this.navCtrl.push(GamePage);
  }


  async eliminarPersonaje(personajeEliminar: Personajes) {
    console.log('Eliminando Personaje');

    let alert = this.alertController.create({
      title: 'Confirmar Eliminacion',
      message: '¿Deseas eliminar por completo tu personaje?, no lo volveras a ver jamas',
      buttons: [
        {
          text: 'Cancelar',
          role: 'cancel',
          handler: () => {
            console.log('Cancel clicked');
          }
        },
        {
          text: 'Hasta Nunca',
          handler: () => {
            console.log('Ok clicked');
            console.log('Elminando: ' + personajeEliminar.nombre + "  " + personajeEliminar.clase + "  " + personajeEliminar.id);
            const batch = this.angularFirestore.firestore.batch();
            const character = this.angularFirestore.doc(`personajes/${personajeEliminar.id}`).ref;
            batch.delete(character);
            batch.commit().then(() => {
              console.log('Personaje ' + personajeEliminar.nombre + ' se ha eliminado');
            }).catch(error => {
              console.log(error);
            })
            this.personaje.nombre = "";
            this.personaje.clase = "";
          }
        }
      ]
    });
    alert.present();
  }

  actualizandoPersonaje(personaje: Personajes) {
    console.log('Actualizando Personaje');
    console.log(personaje.nombre + "  " + personaje.clase + "  " + personaje.id);
    const addModal = this.modalCtrl.create('ModalPersonajePage', { 'personaje': personaje });
    addModal.onDidDismiss(item => {
      if (item) {
        const batch = this.angularFirestore.firestore.batch();
        const character = this.angularFirestore.doc(`personajes/${personaje.id}`).ref;
        console.log('doc: ' + personaje.id);
        batch.update(character, {
          nombre: item.nombre,
          clase: item.clase,
        });
        batch.commit().then(() => {
          console.log('Personaje ' + item.nombre + ' se ha actualizado');
        }).catch(error => {
          console.log(error);
        })
      }
    })
    addModal.present();
  }

  goInventario() {
    let contactModal = this.modalCtrl.create(InventarioPage, { 'uid': this.uid });
    contactModal.present();
  }

  goPersonaje() {
    let contactModal = this.modalCtrl.create(ArmaduraPage, { 'uid': this.uid });
    contactModal.present();
  }

  goMisiones() {
    let contactModal = this.modalCtrl.create(MisionesPage, { 'uid': this.uid });
    contactModal.present();
  }

  goPuntuaciones() {
    let contactModal = this.modalCtrl.create(StatsPage, { 'uid': this.uid });
    contactModal.present();
  }

}
