import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { User } from '../../app/model/user.model';
import { AngularFireAuth } from '../../../node_modules/angularfire2/auth';
import { LoginPage } from '../login/login';

/**
 * Generated class for the RegistrarsePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-registrarse',
  templateUrl: 'registrarse.html',
})
export class RegistrarsePage {

  user = {} as User

  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    private afAuth: AngularFireAuth
  ) {
  }

  async register(user: User){
    try{
      const result = await this.afAuth.auth.createUserWithEmailAndPassword(user.email,user.password);
      this.navCtrl.push(LoginPage);
      console.log(result);
    }catch (e){
      console.error(e);
    }
    
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad RegistrarsePage');
  }

}
