import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MisionesPage } from './misiones';

@NgModule({
  declarations: [
    MisionesPage,
  ],
  imports: [
    IonicPageModule.forChild(MisionesPage),
  ],
})
export class MisionesPageModule {}
