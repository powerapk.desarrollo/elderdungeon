import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController } from 'ionic-angular';
import { AngularFirestore } from 'angularfire2/firestore';

/**
 * Generated class for the MisionesPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-misiones',
  templateUrl: 'misiones.html',
})
export class MisionesPage {

  uid:any;

  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    public viewCtrl: ViewController,
    private angularFirestore: AngularFirestore,
    ) {
      this.uid = this.navParams.get('uid');
      console.log('UserId', this.navParams.get('uid'));
    }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ArmaduraPage');
  }

  cancel(){
    this.viewCtrl.dismiss();
  }

}