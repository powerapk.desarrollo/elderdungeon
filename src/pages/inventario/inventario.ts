import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController, ModalController } from 'ionic-angular';
import { AngularFirestoreCollection, AngularFirestore } from '../../../node_modules/angularfire2/firestore';
import { Inventario } from '../../app/model/inventario.model';
import { UserPreferencesProvider } from '../../providers/user-preferences/user-preferences';
import { AngularFireAuth } from 'angularfire2/auth';

/**
 * Generated class for the InventarioPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-inventario',
  templateUrl: 'inventario.html',
})
export class InventarioPage {

  objeto = {} as Inventario

  inventarioCollection: AngularFirestoreCollection<Inventario>;
  objetosInventario: Inventario[];

  listSize: any;

  //Variables del Usuario
  uid: any;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public viewCtrl: ViewController,
    public modalCtrl: ModalController,
    private angularFirestore: AngularFirestore,
    public userData: UserPreferencesProvider,
    public afAuth: AngularFireAuth,
  ) {
    this.afAuth.authState.subscribe(data => {
      this.userData.uid = data.uid;
    });
  }

  ionViewDidLoad() {
    console.log(`ionViewDidLoad InventarioPage ${this.userData.uid}`);
    this.inventarioCollection = this.angularFirestore.collection('inventario', ref =>
        ref.where('id_usuario', '==', this.userData.uid));

        this.inventarioCollection.snapshotChanges().subscribe(objetosList => {
          this.objetosInventario = objetosList.map(item => {
            return {
              id: item.payload.doc.id,
              id_usuario: item.payload.doc.data().id_usuario,
              id_item: item.payload.doc.data().id_item,
              cantidad: item.payload.doc.data().cantidad
            }
          })
          
        });
    
     
    
  };

  cancel(){
    this.viewCtrl.dismiss();
  }


/**
 *  console.log('uid es'+ this.uid);

      this.inventarioCollection = this.angularFirestore.collection('inventario', ref =>
        ref.where('id_usuario', '==', this.uid));
      this.inventarioCollection.snapshotChanges().subscribe(inventarioList => {
        this.objetosInventario = inventarioList.map(item => {
          return {
            id: item.payload.doc.id,
            id_usuario: item.payload.doc.data().id_usuario,
            id_item: item.payload.doc.data().id_item,
            cantidad: item.payload.doc.data().cantidad
          }
        })
      })
 */
}
