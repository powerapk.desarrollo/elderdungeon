import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController, AlertController } from 'ionic-angular';
import { AngularFirestore, AngularFirestoreCollection } from 'angularfire2/firestore';
import { Personajes } from '../../app/model/personajes.model';

/**
 * Generated class for the StatsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-stats',
  templateUrl: 'stats.html',
})
export class StatsPage {

  personaje = {} as Personajes

  personajesCollection: AngularFirestoreCollection<Personajes>;
  personajes: Personajes[];

  uid:any;

  constructor(
    public alertController: AlertController,
    public navCtrl: NavController, 
    public navParams: NavParams,
    public viewCtrl: ViewController,
    private angularFirestore: AngularFirestore,
    ) {
      
    }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ArmaduraPage');
  }

  ionViewDidEnter() {
 

      this.personajesCollection = this.angularFirestore.collection('personajes');
      this.personajesCollection.snapshotChanges().subscribe(personajesList => {
        this.personajes = personajesList.map(item => {
          return {
            id: item.payload.doc.id,
            nombre: item.payload.doc.data().nombre,
            clase: item.payload.doc.data().clase,
            user: item.payload.doc.data().user,
            raza: item.payload.doc.data().raza,
            recurso: item.payload.doc.data().recurso,
            rank: item.payload.doc.data().rank,
            status: item.payload.doc.data().status,
            level: item.payload.doc.data().level,
          }
        })
      })

  }

  cancel(){
    this.viewCtrl.dismiss();
  }

}
